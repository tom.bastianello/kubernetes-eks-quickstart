# Kubernetes EKS QuickStart
These resources are to be used with the "Zero to Production" article on [minutedevops.com](https://minutedevops.com) 

Click [here](https://minutedevops.com/eks-0-to-100/)  for the complete guide.